package az.ingress.users.dto;

import az.ingress.users.entity.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    @Enumerated(EnumType.STRING)
    UserRole userRole;
    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Long phone;
}

