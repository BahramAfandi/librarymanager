package az.ingress.users.services;

import az.ingress.users.dto.LoginRequest;
import az.ingress.users.dto.LoginResponse;
import az.ingress.users.dto.RegisterDto;
import az.ingress.users.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserServices extends UserDetailsService {
    UserDto getUserId(Long id);

    UserDto createUser(UserDto userDto);

    UserDto updateUser(Long id, UserDto userDto);

    void deleteUser(Long id);

    Page<UserDto> getUserList(Pageable pageable);

    void register(RegisterDto registerDto);

    void registerAuthor(RegisterDto registerDto);

    void registerPublisher(RegisterDto registerDto);

    LoginResponse login(LoginRequest loginDto);

    UserDetails loadUserByUsername(String username);

}
