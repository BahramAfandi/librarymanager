package az.ingress.users.entity;

public enum UserRole {
    ADMIN,
    ROLE_PUBLISHER,
    USER,
    ROLE_AUTHOR;
}
