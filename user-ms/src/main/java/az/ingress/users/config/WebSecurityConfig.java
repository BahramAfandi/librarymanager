package az.ingress.users.config;

import az.ingress.users.config.security.AuthService;
import az.ingress.users.config.security.JwtAuthFilterConfigurerAdapter;
import az.ingress.users.entity.UserRole;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;
import java.util.StringJoiner;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final SecurityProperties securityProperties;
    private final List<AuthService> authServices;

    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().configurationSource(corsConfigurationSource());
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
        http.authorizeRequests()
                .antMatchers(UserRole.USER.name()
                        , UserRole.ROLE_AUTHOR.name()
                        , UserRole.ADMIN.name())
                .permitAll();

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/registration")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/sign-in")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/registration/author")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/auth/registration/publisher")
                .permitAll();

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/user/api/")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers(HttpMethod.POST, "/user/api/create/user")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers(HttpMethod.PUT, "/user/api/update/")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers(HttpMethod.DELETE, "/user/api/delete/")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers(HttpMethod.GET, "/user/api/list")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name());

        http.authorizeRequests()
                .anyRequest()
                .access(authorities(UserRole.USER.name(), UserRole.ADMIN.name()));
        http.apply(new JwtAuthFilterConfigurerAdapter(authServices));
    }

    protected String authority(String role) {
        return "hasAuthority('" + role + "')";
    }

    protected String authority(UserRole role) {
        return "hasAuthority('" + role.name() + "')";
    }

    protected String authorities(Object... roles) {
        StringJoiner joiner = new StringJoiner(" or ");
        for (Object role : roles) {
            if (role instanceof UserRole) {
                joiner.add(authority((UserRole) role));
            } else {
                joiner.add(authority(role.toString()));
            }
        }
        return joiner.toString();
    }

    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}

