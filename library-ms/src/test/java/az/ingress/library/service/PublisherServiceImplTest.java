package az.ingress.library.service;

import az.ingress.library.dto.PublisherDto;
import az.ingress.library.mapper.PublisherMapper;
import az.ingress.library.model.Publisher;
import az.ingress.library.repository.PublisherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PublisherServiceImplTest {
    @Mock
    PublisherRepository publisherRepository;
    @Mock
    PublisherMapper publisherMapper;

    @InjectMocks
    PublisherServiceImpl publisherService;

    private Publisher publisher;
    private PublisherDto publisherDto;

    @BeforeEach
    void setUp() {
        publisher = Publisher
                .builder()
                .id(1L)
                .bookPublisherName("PublisherTest")
                .build();

        publisherDto = PublisherDto
                .builder()
                .bookPublisherName("PublisherTest")
                .build();
    }

    @Test
    void givenIdThenReturnPublisher() {
        when(publisherRepository.findById(1L)).thenReturn(Optional.ofNullable(publisher));
        when(publisherMapper.publisherToPublisherDto(publisher)).thenReturn(publisherDto);

        PublisherDto result = publisherService.getById(1L);

        assertThat(result.getBookPublisherName()).isEqualTo("PublisherTest");

        verify(publisherRepository, times(1)).findById(1L);
        verify(publisherMapper, times(1)).publisherToPublisherDto(publisher);
    }

    @Test
    void givenPublisherDtoThenAddPublisher() {
        when(publisherMapper.publisherDtoToPublisher(publisherDto)).thenReturn(publisher);
        when(publisherRepository.save(publisher)).thenReturn(publisher);
        when(publisherMapper.publisherToPublisherDto(publisher)).thenReturn(publisherDto);

        PublisherDto result = publisherService.addPublisher(publisherDto);

        assertThat(result.getBookPublisherName()).isEqualTo("PublisherTest");

        verify(publisherRepository, times(1)).save(publisher);
        verify(publisherMapper,times(1)).publisherDtoToPublisher(publisherDto);
        verify(publisherMapper,times(1)).publisherToPublisherDto(publisher);
    }

    @Test
    void givenPublisherDtoListThenAddPublisher() {
        when(publisherMapper.publisherDtoListToPublisherList(List.of(publisherDto))).thenReturn(List.of(publisher));
        when(publisherRepository.saveAll(List.of(publisher))).thenReturn(List.of(publisher));
        when(publisherMapper.publisherListToPublisherDtoList(List.of(publisher))).thenReturn(List.of(publisherDto));

        List<PublisherDto> result = publisherService.addPublisherList(List.of(publisherDto));

        assertThat(result.size()).isNotEqualTo(0);

        verify(publisherRepository,times(1)).saveAll(List.of(publisher));
        verify(publisherMapper,times(1)).publisherDtoListToPublisherList(List.of(publisherDto));
        verify(publisherMapper,times(1)).publisherListToPublisherDtoList(List.of(publisher));
    }
}