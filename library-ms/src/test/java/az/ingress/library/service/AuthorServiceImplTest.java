package az.ingress.library.service;

import az.ingress.library.dto.AuthorDto;
import az.ingress.library.mapper.AuthorMapper;
import az.ingress.library.model.Author;
import az.ingress.library.repository.AuthorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest {
    @Mock
    AuthorRepository authorRepository;
    @Mock
    AuthorMapper authorMapper;

    @InjectMocks
    AuthorServiceImpl authorService;


    private Author author;
    private AuthorDto authorDto;

    @BeforeEach
    void setUp() {
        author = Author
                .builder()
                .id(1L)
                .bookAuthorMail("testname@example.com")
                .bookAuthorName("Test Name")
                .build();

        authorDto = AuthorDto
                .builder()
                .bookAuthorMail("testname@example.com")
                .bookAuthorName("Test Name")
                .build();
    }

    @Test
    void givenAuthorIdThenReturnAuthor() {
        when(authorRepository.findById(1L)).thenReturn(Optional.ofNullable(author));
        when(authorMapper.authorToAuthorDto(author)).thenReturn(authorDto);

        AuthorDto result = authorService.getById(1L);

        assertThat(result.getBookAuthorName()).isEqualTo("Test Name");

        verify(authorRepository, times(1)).findById(1L);
        verify(authorMapper,times(1)).authorToAuthorDto(author);
    }

    @Test
    void givenAuthorDtoThenAddAuthorDto() {
        when(authorMapper.authorDtoToAuthor(authorDto)).thenReturn(author);
        when(authorRepository.save(author)).thenReturn(author);
        when(authorMapper.authorToAuthorDto(author)).thenReturn(authorDto);

        AuthorDto result = authorService.addAuthor(authorDto);

        assertThat(result.getBookAuthorName()).isEqualTo("Test Name");

        verify(authorRepository, times(1)).save(author);
        verify(authorMapper,times(1)).authorToAuthorDto(author);
        verify(authorMapper,times(1)).authorDtoToAuthor(authorDto);
    }

    @Test
    void givenAuthorDtoListThenAddAuthorDtoList(){
        when(authorMapper.authorDtoListToAuthorList(List.of(authorDto))).thenReturn(List.of(author));
        when(authorRepository.saveAll(List.of(author))).thenReturn(List.of(author));
        when(authorMapper.authorListToAuthorDtoList(List.of(author))).thenReturn(List.of(authorDto));

        List<AuthorDto> result = authorService.addAuthorList(List.of(authorDto));

        assertThat(result.size()).isNotEqualTo(0);

        verify(authorRepository,times(1)).saveAll(List.of(author));
        verify(authorMapper,times(1)).authorDtoListToAuthorList(List.of(authorDto));
        verify(authorMapper,times(1)).authorListToAuthorDtoList(List.of(author));
    }
}