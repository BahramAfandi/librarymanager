package az.ingress.library.service;

import az.ingress.library.dto.AuthorDto;
import az.ingress.library.dto.BookDto;
import az.ingress.library.dto.BookRequestDto;
import az.ingress.library.dto.PublisherDto;
import az.ingress.library.exception.BookNotFoundException;
import az.ingress.library.mapper.AuthorMapper;
import az.ingress.library.mapper.BookMapper;
import az.ingress.library.model.Author;
import az.ingress.library.model.Book;
import az.ingress.library.model.Publisher;
import az.ingress.library.repository.AuthorRepository;
import az.ingress.library.repository.BookRepository;
import az.ingress.library.repository.PublisherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {
    @Mock
    BookRepository bookRepository;
    @Mock
    AuthorRepository authorRepository;
    @Mock
    PublisherRepository publisherRepository;
    @Mock
    AuthorServiceImpl authorService;
    @Mock
    PublisherServiceImpl publisherService;
    @Mock
    BookMapper bookMapper;
    @Mock
    AuthorMapper authorMapper;
    @InjectMocks
    BookServiceImpl bookService;

    private Book book;
    private BookDto bookDto;
    private BookRequestDto bookRequestDto;
    private Page<Book> bookPage;
    private Author author;
    private AuthorDto authorDto;
    private Publisher publisher;
    private PublisherDto publisherDto;
    private List<BookDto> bookDtoList;

    private List<Book> bookList;
    private Page<BookDto> bookDtoPage;
    private ModelMapper modelMapper;


    @BeforeEach
    void setUp() {
        author = Author
                .builder()
                .id(1L)
                .bookAuthorMail("testname@example.com")
                .bookAuthorName("Test Name")
                .build();

        authorDto = AuthorDto
                .builder()
                .bookAuthorMail("testname@example.com")
                .bookAuthorName("Test Name")
                .build();

        publisher = Publisher
                .builder()
                .id(212L)
                .bookPublisherName("testpublisher")
                .build();

        publisherDto = PublisherDto
                .builder()
                .bookPublisherName("testpublisher")
                .build();

        book = Book.
                builder()
                .id(218L)
                .bookName("BookNameTest")
                .bookPrice(BigDecimal.valueOf(27.5))
                .bookPublisher(publisher)
                .bookAuthor(author)
                .bookCoverType("BookCoverTypeTest")
                .bookShelf("5TEST")
                .bookGenre("BookGenreTest")
                .bookQuantity(19)
                .build();

        bookDto = BookDto.
                builder()
                .bookName("BookNameTest")
                .bookPrice(BigDecimal.valueOf(27.5))
                .bookPublisher(publisherDto)
                .bookAuthor(authorDto)
                .bookCoverType("BookCoverTypeTest")
                .bookShelf("5TEST")
                .bookGenre("BookGenreTest")
                .bookQuantity(19)
                .build();

        bookRequestDto = BookRequestDto.
                builder()
                .bookName("BookNameTest")
                .bookPrice(BigDecimal.valueOf(27.5))
                .bookPublisherId(212L)
                .bookAuthorId(1L)
                .bookCoverType("BookCoverTypeTest")
                .bookShelf("5TEST")
                .bookGenre("BookGenreTest")
                .bookQuantity(19)
                .build();

        bookDtoList = new ArrayList<>();
        bookDtoList.add(bookDto);
        bookDtoPage = new PageImpl<>(bookDtoList);

        bookList = new ArrayList<>();
        bookList.add(book);
        bookPage = new PageImpl<>(bookList);
    }


    @Test
    void givenPageThenReturnAllBookDto() {
        PageRequest pr = PageRequest.of(0, 10);

        when(bookRepository.findAll(pr)).thenReturn(bookPage);
        when(bookMapper.bookPageToBookDtoPage(bookPage)).thenReturn(bookDtoPage);

        Page<BookDto> result = bookService.getAllModel(pr);

        assertThat(result).isEqualTo(bookDtoPage);

        verify(bookRepository, times(1)).findAll(pr);
        verify(bookMapper, times(1)).bookPageToBookDtoPage(bookPage);
    }

    @Test
    void givenIdThenReturnBook() {
        when(bookRepository.findById(218L)).thenReturn(Optional.ofNullable(book));
        when(bookMapper.bookToBookDto(book)).thenReturn(bookDto);

        BookDto result = bookService.getById(218L);

        assertThat(result.getBookName()).isEqualTo("BookNameTest");

        verify(bookRepository, times(1)).findById(218L);
        verify(bookMapper, times(1)).bookToBookDto(book);
    }

    @Test
    void givenBookNameThenReturnBook() {
        when(bookRepository.findByBookName("BookNameTest")).thenReturn(Optional.ofNullable(book));
        when(bookMapper.bookToBookDto((book))).thenReturn(bookDto);

        BookDto result = bookService.getByBookName("BookNameTest");

        assertThat(result.getBookName()).isEqualTo("BookNameTest");

        verify(bookRepository, times(1)).findByBookName("BookNameTest");
        verify(bookMapper, times(1)).bookToBookDto(book);
    }

    @Test
    void givenBookAuthorIdThenReturnBookDtoPage() {
        PageRequest pr = PageRequest.of(0, 10);

        when(bookRepository.findBooksByBookAuthorId(pr, 1L)).thenReturn(bookPage);
        when(bookMapper.bookPageToBookDtoPage(bookPage)).thenReturn(bookDtoPage);

        Page<BookDto> result = bookService.getByBookAuthor(pr, 1L);

        assertThat(result).isEqualTo(bookDtoPage);

        verify(bookRepository, times(1)).findBooksByBookAuthorId(pr, 1L);
        verify(bookMapper, times(1)).bookPageToBookDtoPage(bookPage);
    }

    @Test
    void givenBookPublisherIdThenReturnBookDtoPage() {
        PageRequest pr = PageRequest.of(0, 10);

        when(bookRepository.findAllByBookPublisherId(pr, 212L)).thenReturn(bookPage);
        when(bookMapper.bookPageToBookDtoPage(bookPage)).thenReturn(bookDtoPage);

        Page<BookDto> result = bookService.getByBookPublisher(pr, 212L);

        assertThat(result).isEqualTo(bookDtoPage);

        verify(bookRepository, times(1)).findAllByBookPublisherId(pr, 212L);
        verify(bookMapper, times(1)).bookPageToBookDtoPage(bookPage);
    }

    @Test
    void givenPageAndMinAndMaxPriceThenReturnPageOfBookDto() {
        PageRequest pr = PageRequest.of(0,10);
        BigDecimal minPrice = BigDecimal.valueOf(20.1);
        BigDecimal maxPrice = BigDecimal.valueOf(30.5);

        when(bookRepository.findAllByMinAndMaxBookPrice(pr, minPrice, maxPrice)).thenReturn(bookPage);
        when(bookMapper.bookPageToBookDtoPage(bookPage)).thenReturn(bookDtoPage);

        Page<BookDto> result = bookService.getAllByMinAndMaxBookPrice(pr, minPrice, maxPrice);

        assertThat(result).isEqualTo(bookDtoPage);

        verify(bookRepository, times(1)).findAllByMinAndMaxBookPrice(pr, minPrice, maxPrice);
        verify(bookMapper, times(1)).bookPageToBookDtoPage(bookPage);
    }

    @Test
    void givenBookRequestDtoThenReturnBookDto() {
        when(bookMapper.bookRequestDtoToBook(bookRequestDto)).thenReturn(book);
        when(authorRepository.findById(1L)).thenReturn(Optional.ofNullable(author));
        when(publisherRepository.findById(212L)).thenReturn(Optional.ofNullable(publisher));
        when(bookRepository.save(book)).thenReturn(book);
        when(bookMapper.bookToBookDto(book)).thenReturn(bookDto);

        BookDto result = bookService.addBook(bookRequestDto);

        assertThat(result).isEqualTo(bookDto);

        verify(bookMapper,times(1)).bookRequestDtoToBook(bookRequestDto);
        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, times(1)).findById(212L);
        verify(bookRepository, times(1)).save(book);
        verify(bookMapper, times(1)).bookToBookDto(book);
    }

    @Test
    void addBookList() {
        when(bookMapper.bookRequestDtoToBook(bookRequestDto)).thenReturn(book);
        when(authorRepository.findById(1L)).thenReturn(Optional.ofNullable(author));
        when(publisherRepository.findById(212L)).thenReturn(Optional.ofNullable(publisher));
        when(bookRepository.saveAll(List.of(book))).thenReturn(List.of(book));
        when(bookMapper.bookListToBookDtoList(List.of(book))).thenReturn(List.of(bookDto));

        List<BookDto> result = bookService.addBookList(List.of(bookRequestDto));

        assertThat(result).size().isNotZero();

        verify(bookMapper,times(1)).bookRequestDtoToBook(bookRequestDto);
        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, times(1)).findById(212L);
        verify(bookRepository, times(1)).saveAll(List.of(book));
        verify(bookMapper, times(1)).bookListToBookDtoList(List.of(book));
    }

    @Test
    void givenBookIdThenRemoveBook() {
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(book));
        doNothing().when(bookRepository).deleteById(1L);

        bookService.removeBookById(1L);

        verify(bookRepository, times(1)).deleteById(1L);
    }

    @Test
    void update() {
        when(bookRepository.findById(218L)).thenReturn(Optional.ofNullable(book));
        when(authorRepository.findById(1L)).thenReturn(Optional.ofNullable(author));
        when(publisherRepository.findById(212L)).thenReturn(Optional.ofNullable(publisher));
        when(bookRepository.save(book)).thenReturn(book);
        when(bookMapper.bookToBookDto(book)).thenReturn(bookDto);

        BookDto result = bookService.update(218L, bookRequestDto);

        assertThat(result).isEqualTo(bookDto);

        verify(bookRepository,times(1)).findById(218L);
        verify(authorRepository,times(1)).findById(1L);
        verify(publisherRepository,times(1)).findById(212L);
        verify(bookRepository,times(1)).save(book);
        verify(bookMapper,times(1)).bookToBookDto(book);
    }

    @Test
    void authorAndPublisherSetter() {
        when(bookMapper.bookRequestDtoToBook(bookRequestDto)).thenReturn(book);
        when(authorRepository.findById(1L)).thenReturn(Optional.ofNullable(author));
        when(publisherRepository.findById(212L)).thenReturn(Optional.ofNullable(publisher));

        Book result = bookService.authorAndPublisherSetter(bookRequestDto);

        assertThat(result).isEqualTo(book);

        verify(bookMapper, times(1)).bookRequestDtoToBook(bookRequestDto);
        verify(authorRepository,times(1)).findById(1L);
        verify(publisherRepository,times(1)).findById(212L);
    }
}