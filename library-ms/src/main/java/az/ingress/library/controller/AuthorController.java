package az.ingress.library.controller;

import az.ingress.library.dto.AuthorDto;
import az.ingress.library.dto.PublisherDto;
import az.ingress.library.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/author")


public class AuthorController {
    private final AuthorService authorService;

    @GetMapping(value = "/id/{id}")
    AuthorDto getById(@PathVariable long id){
        return authorService.getById(id);
    }

    @PostMapping(value = "/post")
    AuthorDto postAuthor(@RequestBody AuthorDto authorDto){
        return authorService.addAuthor(authorDto);
    }

    @PostMapping(value = "/post-list")
    List<AuthorDto> postAuthorList (@RequestBody List<AuthorDto> authorDtoList){
        return authorService.addAuthorList(authorDtoList);
    }
}


