package az.ingress.library.controller;

import az.ingress.library.dto.BookDto;
import az.ingress.library.dto.BookRequestDto;
import az.ingress.library.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/book")
@Slf4j
public class BookController {
    private final BookService bookService;

    @GetMapping(value = "/get-all")
    public Page<BookDto> getAllBook(Pageable pageable) {
        return bookService.getAllModel(pageable);
    }

    @GetMapping(value = "/id/{id}")
    public BookDto getById(@PathVariable(name = "id") long id) {
        return bookService.getById(id);
    }


    @GetMapping(value = "/by-name")
    public BookDto getByBookName(@PathParam("bookName") String bookName) {
        return bookService.getByBookName(bookName);
    }

    @GetMapping(value = "/by-author-id/{bookAuthorId}")
    public Page<BookDto> getByBookAuthor(Pageable pageable, @PathVariable long bookAuthorId) {
        return bookService.getByBookAuthor(pageable, bookAuthorId);
    }

    @GetMapping(value = "/price")
    public Page<BookDto> getAllByMinAndMaxBookPrice(Pageable pageable,
                                                    @RequestParam(value = "minPrice") BigDecimal minPrice,
                                                    @RequestParam(value = "maxPrice") BigDecimal maxPrice) {
        return bookService.getAllByMinAndMaxBookPrice(pageable, minPrice, maxPrice);
    }

    @GetMapping(value = "/by-publisher-id/{bookPublisherId}")
    public Page<BookDto> getByBookPublisher(Pageable pageable, @PathVariable long bookPublisherId) {
        return bookService.getByBookPublisher(pageable, bookPublisherId);
    }

    @PostMapping(value = "/post")
    public BookDto postBook(@RequestBody BookRequestDto bookRequestDto) {
        return bookService.addBook(bookRequestDto);
    }

    @PostMapping(value = "/post-list")
    public List<BookDto> postBook(@RequestBody List<BookRequestDto> bookRequestDtoList) {
        return bookService.addBookList(bookRequestDtoList);
    }

    @PutMapping(value = "/update/{id}")
    public BookDto updateBook(@PathVariable long id,
                              @RequestBody BookRequestDto bookDto) {
        return bookService.update(id, bookDto);
    }

    @DeleteMapping(value = "/delete/{id}")
    void deleteBookByBookName(@PathVariable long id) {
        bookService.removeBookById(id);
    }
}
