package az.ingress.library.controller;

import az.ingress.library.dto.PublisherDto;
import az.ingress.library.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/publisher")

public class PublisherController {
    private final PublisherService publisherService;

    @GetMapping(value = "/id/{id}")
    PublisherDto getById(@PathVariable long id){
        return publisherService.getById(id);
    }

    @PostMapping(value = "/post")
    PublisherDto postPublisher(@RequestBody PublisherDto publisherDto) {
        return publisherService.addPublisher(publisherDto);
    }

    @PostMapping(value = "/post-list")
    List<PublisherDto> postPublisherList(@RequestBody List<PublisherDto> publisherDtoList) {
        return publisherService.addPublisherList(publisherDtoList);
    }
}


