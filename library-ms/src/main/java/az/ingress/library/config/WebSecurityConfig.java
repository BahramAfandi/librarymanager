package az.ingress.library.config;

import az.ingress.library.config.security.AuthService;
import az.ingress.library.config.security.JwtAuthFilterConfigurerAdapter;
import az.ingress.library.config.security.SecurityProperties;
import az.ingress.users.entity.UserRole;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;
import java.util.StringJoiner;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final SecurityProperties securityProperties;
    private final List<AuthService> authServices;


    @Override
    @SneakyThrows
    protected void configure(HttpSecurity http) {
        http.csrf().disable()
                .cors().configurationSource(corsConfigurationSource())
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()

                .antMatchers(HttpMethod.GET, "/book/get-all")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.GET, "/book/id/{id}")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.GET, "/book/by-name")
                .hasAnyAuthority(UserRole.USER.name(), UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.POST, "/book/post")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.POST, "/book/post-list")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.POST, "/author/post-list")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())
                //@ToDo
                .antMatchers( "/author/**")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())


                .antMatchers( "/publisher/**")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_PUBLISHER.name())




                .antMatchers(HttpMethod.POST, "/publisher/post-list")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.PUT, "/book/update/{id}")
                .hasAnyAuthority(UserRole.ROLE_AUTHOR.name(),UserRole.ROLE_PUBLISHER.name())

                .antMatchers(HttpMethod.DELETE, "/book/delete/{id}")
                .hasAnyAuthority(UserRole.ROLE_PUBLISHER.name(),UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.GET, "/book/price")
                .hasAnyAuthority(UserRole.ROLE_AUTHOR.name(),UserRole.ROLE_PUBLISHER.name())

                .antMatchers(HttpMethod.GET, "/book/by-author-id/{bookAuthorId}")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_AUTHOR.name())

                .antMatchers(HttpMethod.GET, "/book/by-publisher-id/{bookPublisherId}")
                .hasAnyAuthority(UserRole.USER.name(),UserRole.ROLE_PUBLISHER.name());

        http.authorizeRequests()
                .anyRequest()
                .access(authorities(UserRole.USER.name(), UserRole.ADMIN.name()));
        http.apply(new JwtAuthFilterConfigurerAdapter(authServices));
    }

    protected String authority(String role) {
        return "hasAuthority('" + role + "')";
    }

    protected String authority(UserRole role) {
        return "hasAuthority('" + role.name() + "')";
    }

    protected String authorities(Object... roles) {
        StringJoiner joiner = new StringJoiner(" or ");
        for (Object role : roles) {
            if (role instanceof UserRole) {
                joiner.add(authority((UserRole) role));
            } else {
                joiner.add(authority(role.toString()));
            }
        }
        return joiner.toString();
    }

    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
