package az.ingress.library.exception;

import org.modelmapper.spi.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MyExceptionHandler {
    @ExceptionHandler(value = {BookNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handleBookRequestException(Exception ex) {
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler(value = {AuthorNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handleAuthorRequestException(Exception ex) {
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler(value = {PublisherNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handlePublisherRequestException(Exception ex) {
        return new ErrorMessage(ex.getMessage());
    }
}