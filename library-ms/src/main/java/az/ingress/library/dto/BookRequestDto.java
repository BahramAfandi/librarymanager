package az.ingress.library.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookRequestDto {
    String bookName;
    BigDecimal bookPrice;
    Long bookPublisherId;
    Long bookAuthorId;
    String bookCoverType;
    String bookShelf;
    String bookGenre;
    Integer bookQuantity;
}
