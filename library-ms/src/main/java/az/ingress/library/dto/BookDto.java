package az.ingress.library.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookDto {
    String bookName;
    BigDecimal bookPrice;
    PublisherDto bookPublisher;
    AuthorDto bookAuthor;
    String bookCoverType;
    String bookShelf;
    String bookGenre;
    Integer bookQuantity;
}