package az.ingress.library.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    String bookAuthorName;
    String bookAuthorMail;
}
