package az.ingress.library.service;

import az.ingress.library.dto.AuthorDto;
import az.ingress.library.exception.AuthorNotFoundException;
import az.ingress.library.mapper.AuthorMapper;
import az.ingress.library.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    public AuthorDto getById(Long id) {
        return authorMapper.authorToAuthorDto(authorRepository.findById(id).orElseThrow(() ->
                new AuthorNotFoundException("Author with id " + id + " not found")));
    }

    @Override
    public AuthorDto addAuthor(AuthorDto authorDto) {
        return authorMapper.authorToAuthorDto(authorRepository.save(authorMapper.authorDtoToAuthor(authorDto)));
    }

    @Override
    public List<AuthorDto> addAuthorList(List<AuthorDto> authorDtoList) {
        return authorMapper.authorListToAuthorDtoList(authorRepository.saveAll(
                authorMapper.authorDtoListToAuthorList(authorDtoList))
        );
    }
}
