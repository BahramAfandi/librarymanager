package az.ingress.library.service;

import az.ingress.library.dto.BookDto;
import az.ingress.library.dto.BookRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface BookService {
    BookDto getById(Long id); // test ok

    BookDto getByBookName(String bookName); // test ok

    Page<BookDto> getByBookAuthor(Pageable pageable, Long bookAuthorId); // test ok

    Page<BookDto> getAllModel(Pageable pageable);

    Page<BookDto> getAllByMinAndMaxBookPrice(Pageable pageable, BigDecimal minPrice, BigDecimal maxPrice);

    Page<BookDto> getByBookPublisher(Pageable pageable, Long bookPublisherId);

    BookDto addBook(BookRequestDto bookRequestDto);

    List<BookDto> addBookList(List<BookRequestDto> bookRequestDtoList);

    void removeBookById(Long id);

    BookDto update(Long bookId, BookRequestDto bookDto);
}
