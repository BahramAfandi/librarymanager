package az.ingress.library.service;

import az.ingress.library.dto.BookDto;
import az.ingress.library.dto.BookRequestDto;
import az.ingress.library.exception.AuthorNotFoundException;
import az.ingress.library.exception.BookNotFoundException;
import az.ingress.library.exception.PublisherNotFoundException;
import az.ingress.library.mapper.AuthorMapper;
import az.ingress.library.mapper.BookMapper;
import az.ingress.library.mapper.PublisherMapper;
import az.ingress.library.model.Author;
import az.ingress.library.model.Book;
import az.ingress.library.model.Publisher;
import az.ingress.library.repository.AuthorRepository;
import az.ingress.library.repository.BookRepository;
import az.ingress.library.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;
    private final AuthorService authorService;
    private final PublisherService publisherService;
    private final ModelMapper modelMapper;
    private final PublisherMapper publisherMapper;

    @Override
    public Page<BookDto> getAllModel(Pageable pageable) {
        return bookMapper.bookPageToBookDtoPage(bookRepository.findAll(pageable));
    }

    @Override
    public BookDto getById(Long id) {
        return bookMapper.bookToBookDto(bookRepository.findById(id).orElseThrow(() ->
                new BookNotFoundException("Book with id " + id + " not found")));
    }

    @Override
    public BookDto getByBookName(String bookName) {
        return bookMapper.bookToBookDto(bookRepository.findByBookName(bookName).orElseThrow(() ->
                new BookNotFoundException("Book by name " + bookName + " not found")
        ));
    }

    @Override
    public Page<BookDto> getByBookAuthor(Pageable pageable, Long bookAuthorId) {
        return bookMapper.bookPageToBookDtoPage(bookRepository
                .findBooksByBookAuthorId(pageable, bookAuthorId));
    }


    @Override
    public Page<BookDto> getByBookPublisher(Pageable pageable, Long bookPublisherId) {

        return bookMapper.bookPageToBookDtoPage(bookRepository
                .findAllByBookPublisherId(pageable, bookPublisherId));
    }

    @Override
    public Page<BookDto> getAllByMinAndMaxBookPrice(Pageable pageable, BigDecimal minPrice, BigDecimal maxPrice) {

        return bookMapper.bookPageToBookDtoPage(bookRepository
                .findAllByMinAndMaxBookPrice(pageable, minPrice, maxPrice));


    }

    @Override
    public BookDto addBook(BookRequestDto bookRequestDto) {
        Book postBook = authorAndPublisherSetter(bookRequestDto);
        return bookMapper.bookToBookDto(bookRepository.save(postBook));
    }

    public List<BookDto> addBookList(List<BookRequestDto> bookRequestDtoList) {
        List<Book> bookList = new ArrayList<>();
        for (BookRequestDto bookRequestDto : bookRequestDtoList) {
            Book book = bookMapper.bookRequestDtoToBook(bookRequestDto);
            Author author = authorRepository.findById(bookRequestDto.getBookAuthorId())
                    .orElseThrow(() -> new AuthorNotFoundException("Author Not Found!"));
            Publisher publisher = publisherRepository.findById(bookRequestDto.getBookPublisherId())
                    .orElseThrow(() -> new PublisherNotFoundException("Publisher Not Found!"));
            book.setBookAuthor(author);
            book.setBookPublisher(publisher);
            bookList.add(book);
        }

        return bookMapper.bookListToBookDtoList(bookRepository.saveAll(bookList));
    }


    @Override
    public void removeBookById(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            bookRepository.deleteById(id);
        } else throw new BookNotFoundException("Book with id " + id + " is not found");
    }

    @Override
    public BookDto update(Long bookId, BookRequestDto bookRequestDto) {
        Book book = bookRepository.findById(bookId).orElseThrow(() ->
                new BookNotFoundException(
                        "Book with id " + bookId + " not found"));
        Author author = authorRepository.findById(bookRequestDto.getBookAuthorId())
                .orElseThrow(() ->
                        new AuthorNotFoundException(
                                "Author with id " + bookRequestDto.getBookAuthorId() + " not found"));
        Publisher publisher = publisherRepository.findById(bookRequestDto.getBookPublisherId())
                .orElseThrow(() ->
                        new PublisherNotFoundException(
                                "Publisher with id " + bookRequestDto.getBookPublisherId() + " not found"));

        book.setBookName(bookRequestDto.getBookName());
        book.setBookPrice(bookRequestDto.getBookPrice());
        book.setBookCoverType(bookRequestDto.getBookCoverType());
        book.setBookShelf(bookRequestDto.getBookShelf());
        book.setBookGenre(bookRequestDto.getBookGenre());
        book.setBookQuantity(bookRequestDto.getBookQuantity());
        book.setBookAuthor(author);
        book.setBookPublisher(publisher);

        return bookMapper.bookToBookDto(bookRepository.save(book));
    }

    public Book authorAndPublisherSetter(BookRequestDto bookRequestDto) {
        Book postBook = bookMapper.bookRequestDtoToBook(bookRequestDto);
        postBook.setBookAuthor(authorRepository
                .findById(bookRequestDto.getBookAuthorId()).orElseThrow(() ->
                        new AuthorNotFoundException("Author with given id not found")));
        postBook.setBookPublisher(publisherRepository
                .findById(bookRequestDto.getBookPublisherId()).orElseThrow(() ->
                        new PublisherNotFoundException("Publisher with given id not found")));
        return postBook;
    }
}
