package az.ingress.library.service;

import az.ingress.library.dto.PublisherDto;
import az.ingress.library.exception.PublisherNotFoundException;
import az.ingress.library.mapper.PublisherMapper;
import az.ingress.library.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository publisherRepository;
    private final PublisherMapper publisherMapper;

    @Override
    public PublisherDto getById(Long id) {
        return publisherMapper.publisherToPublisherDto(publisherRepository.findById(id).orElseThrow(() ->
                new PublisherNotFoundException("Publisher with id " + id + " Not Found!")));
    }

    @Override
    public PublisherDto addPublisher(PublisherDto publisherDto) {
        return publisherMapper.publisherToPublisherDto(
                publisherRepository.save(publisherMapper.publisherDtoToPublisher(publisherDto))
        );
    }

    @Override
    public List<PublisherDto> addPublisherList(List<PublisherDto> publisherDtoList) {
        return publisherMapper.publisherListToPublisherDtoList(
                publisherRepository.saveAll(publisherMapper.publisherDtoListToPublisherList(publisherDtoList))
        );
    }
}
