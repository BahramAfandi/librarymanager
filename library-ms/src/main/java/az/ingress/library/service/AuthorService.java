package az.ingress.library.service;

import az.ingress.library.dto.AuthorDto;

import java.util.List;

public interface AuthorService {
    AuthorDto addAuthor(AuthorDto authorDto);
    List<AuthorDto> addAuthorList (List<AuthorDto> authorDtoList);

    AuthorDto getById(Long id);
}
