package az.ingress.library.service;

import az.ingress.library.dto.PublisherDto;

import java.util.List;

public interface PublisherService {

    PublisherDto getById(Long id);
    PublisherDto addPublisher(PublisherDto publisherDto);

    List<PublisherDto> addPublisherList(List<PublisherDto> publisherDtoList);
}
