package az.ingress.library.mapper;

import az.ingress.library.dto.PublisherDto;
import az.ingress.library.model.Publisher;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "Spring")
public interface PublisherMapper {
    Publisher publisherDtoToPublisher(PublisherDto publisherDto);

    PublisherDto publisherToPublisherDto(Publisher publisher);

    List<PublisherDto> publisherListToPublisherDtoList(List<Publisher> publisherList);
    List<Publisher> publisherDtoListToPublisherList(List<PublisherDto> publisherDtoList);

}
