package az.ingress.library.mapper;

import az.ingress.library.dto.AuthorDto;
import az.ingress.library.model.Author;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "Spring")
public interface AuthorMapper {
    Author authorDtoToAuthor(AuthorDto authorDto);

    AuthorDto authorToAuthorDto(Author author);

    List<Author> authorDtoListToAuthorList (List<AuthorDto> authorDtoList);
    List<AuthorDto> authorListToAuthorDtoList (List<Author> authorList);

}
