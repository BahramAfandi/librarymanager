package az.ingress.library.mapper;

import az.ingress.library.dto.BookDto;
import az.ingress.library.dto.BookRequestDto;
import az.ingress.library.model.Book;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "Spring")
public interface BookMapper {
    Book bookDtoToBook(BookDto bookDto);

    BookDto bookToBookDto(Book book);

    Book bookRequestDtoToBook (BookRequestDto bookRequestDto);
    BookRequestDto bookToBookRequestDto(Book book);

    List<Book> bookDtoListToBookList(List<BookDto> bookDtoList);

    List<BookDto> bookListToBookDtoList(List<Book> books);


//    Page<Book> bookDtoPageToBookPage(Page<BookDto> bookDtoPage);

    default Page<Book> bookDtoPageToBookPage(Page<BookDto> bookDtoPage) {
        List<Book> bookList = bookDtoPage.stream()
                .map(this::bookDtoToBook)
                .collect(Collectors.toList());

        return new PageImpl<>(bookList, bookDtoPage.getPageable(), bookDtoPage.getTotalElements());
    }

    default Page<BookDto> bookPageToBookDtoPage(Page<Book> bookPage) {
        List<BookDto> bookDtoList = bookPage.stream()
                .map(this::bookToBookDto)
                .collect(Collectors.toList());

        return new PageImpl<>(bookDtoList, bookPage.getPageable(), bookPage.getTotalElements());
    }

}
