package az.ingress.library.repository;

import az.ingress.library.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findById(Long id);

    Optional<Book> findByBookName(String bookName);

    Page<Book> findBooksByBookAuthorId(Pageable pageable, Long id);

    Page<Book> findAllByBookPublisherId(Pageable pageable, Long id);

    @Query(value = "SELECT * FROM book WHERE book_Price > :minPrice AND book_Price < :maxPrice", nativeQuery = true)
    Page<Book> findAllByMinAndMaxBookPrice(Pageable pageable,
                                           @Param("minPrice") BigDecimal minPrice,
                                           @Param("maxPrice") BigDecimal maxPrice);
}