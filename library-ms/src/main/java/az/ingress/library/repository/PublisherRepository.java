package az.ingress.library.repository;

import az.ingress.library.model.Author;
import az.ingress.library.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {

    Optional<Publisher> findById(Long id);
}